const horseRaceGame = new HorseRacing();

function HorseRacing() {
  const PLAYER_DEFAULT_BANK = 1000;
  this.horses = ['Лошадь 1', 'Лошадь 2', 'Лошадь 3', 'Лошадь 4'].map(
    (name) => new Horse(name)
  );
  this.player = new Player(PLAYER_DEFAULT_BANK);
  this.wagers = [];
  this.newBank = PLAYER_DEFAULT_BANK;
  this.clearWagers = function () {
    // очистить ставки
    horseRaceGame.wagers = [];
  };

  this.betValidation = function (bet) {
    const betToCheck = Number(bet);

    if (horseRaceGame.player.account < bet) {
      throw new Error('Недостаточно средств');
    }

    if (Number.isNaN(betToCheck) || bet < 1) {
      throw new Error('Введите число больше 1');
    }
  };

  this.calcWin = function (winnerHorse) {
    let amountOfWin = 0;

    horseRaceGame.wagers.forEach((wager) => {
      if (wager.name === winnerHorse) {
        amountOfWin += wager.bet * 2;
        horseRaceGame.player.account += wager.bet * 2;
      }
    });

    return (
      amountOfWin &&
      console.log(
        `Вы выиграли ${amountOfWin}, текущий счет ${this.player.account}`
      )
    );
  };

  this.getFinishTime = function (min, max) {
    return Math.floor(min + Math.random() * (max + 1 - min));
  };

  function Horse(name) {
    this.name = name;
    this.run = function () {
      // вернуть Promise, который будет автоматически выполнен через случайное время
      const timer = horseRaceGame.getFinishTime(500, 3000);
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve({ name, timer });
        }, timer);
      });
    };
  }

  function Player(bank) {
    this.account = bank;
  }

  // здесь вы можете дописать различные вспомогательные функции
  // например, для проверки ставок, изменения счета игрока и т.д.
}

function showHorses() {
  // вывести список лошадей
  horseRaceGame.horses.map((horse) => console.log(horse.name));
}

function showAccount() {
  // показать сумму на счету игрока
  console.log(`Текущая сумма на счету: ${horseRaceGame.player.account}`);
}

function setWager(name, sumToBet) {
  // сделать ставку на лошадь в следующем забеге
  horseRaceGame.betValidation(sumToBet);

  horseRaceGame.wagers.push({
    name: name,
    bet: sumToBet,
  });

  horseRaceGame.player.account -= sumToBet;
  console.log(
    `Ставка ${sumToBet} на ${name} принята. Текущий баланс: ${horseRaceGame.player.account}`
  );
}

function startRacing() {
  // начать забег
  let winner = null;
  console.log('Начало забега');

  const raceStarted = horseRaceGame.horses.map((horse) =>
    horse.run().then((finished) => {
      console.log(`${horse.name} - ${finished.timer}`);
      return finished.name;
    })
  );

  Promise.race(raceStarted).then((firstFinished) => (winner = firstFinished));

  Promise.all(raceStarted)
    .then(() => {
      horseRaceGame.calcWin(winner);
    })
    .finally(() => {
      console.log('Забег окончен');
      horseRaceGame.clearWagers();
    });
}

function newGame() {
  // восстановить исходный счет игрока
  console.log('Начало новой игры');
  horseRaceGame.player.account = horseRaceGame.newBank;
}
